import './less/core.less';

function FluxEditor(selector, options) {
  this.fluxEditorUniqIndex = 0;
  this.version = '0.9.5.a';
  this.contents = [];
  this.registeredPlugins = {}
  this.selector = selector;
  this.$container = $(selector);
  this.$container.addClass('flux-editor')
  FluxEditorEvents(this);

  this.defaults = {
    plugins: {},
    events: {
      onInit: function(instance) {},
      onNewContent: function(args) {},
      onRenderEditable: function(instance) {},
      onClickInside: function(instance) {},
      onClickOutside: function(instance) {}
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  this.enable();
  this.emit('onInit', this);

}

FluxEditor.prototype.enable = function() {
  this.registerPlugIns();
  this.registerEvents();
  this.fetchContents();
  this.renderEditableContents();
}

FluxEditor.prototype.registerPlugIns = function() {
  var _this = this;
  if (typeof this.settings.plugins == 'object') {
    $.each(this.settings.plugins, function(i, p) {
      //  TODO check if required methods exist
      var selectors = [];
      $.each(p.getSelectors(), function(j, s) {
        selectors.push(`${_this.selector}>${s}`)
      })
      _this.registeredPlugins[selectors.join(', ')] = {
        type: 'text',
        instance: p
      }
    });
  }
}

FluxEditor.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }

  $(window).resize(function() {
    _this.removeTooltip();
  });

  $(window).scroll(function() {
    _this.removeTooltip();
  });

  $(document).mousedown(function(e) {
    var clickInside = false;
    $(e.target).parents().each(function(k, p) {
      var $p = $(this);
      if ($p.hasClass('flux-editor') || $p.hasClass('flux-editor-tooltip-box')) {
        clickInside = true;
      }
    });
    if (clickInside) {
      _this.emit('onClickInside', this);
    } else {
      _this.emit('onClickOutside', this);
      _this.removeTooltip();
    }
  });
}

FluxEditor.prototype.fetchContents = function() {
  var _this = this;
  var undeditableElements = [];

  this.$container.find('>*').each(function(i, el) {
    el.fluxElementIndex = i;
    el.fluxElementId = _this.getFluxEditorUniqId();
    undeditableElements.push(i);
  });

  $.each(Object.keys(this.registeredPlugins), function(k, s) {
    $(s).each(function(i, el){
      var plugin = _this.registeredPlugins[s];
      _this.contents[el.fluxElementIndex] = plugin.instance.getContent(el);
      _this.contents[el.fluxElementIndex].fluxElementId = el.fluxElementId;
      _this.contents[el.fluxElementIndex].plugin = plugin.instance;
      delete undeditableElements[el.fluxElementIndex];
    });
  });

  _this.contents = _this.contents.filter(function(n){ return n != undefined });
  undeditableElements = undeditableElements.filter(function(n){ return n != undefined });

  if (undeditableElements) {
    var undeditable = new FluxEditorUneditable();
    $.each(undeditableElements, function(k, index) {
      var $el = _this.$container.find('>:eq(' + index +')');
      _this.contents.splice(index, 0, undeditable.getContent($el[0]));
      _this.contents[index].fluxElementId = $el[0].fluxElementId;
      _this.contents[index].plugin = undeditable;
    })
  }
}

FluxEditor.prototype.renderEditableContents = function() {
  var _this = this;
  this.$container.empty();

  $.each(this.contents, function(i, c) {
    var $el = c.plugin.getEditableElement(c, _this);
    $el.attr('id', 'flux-editor-element_' + c.fluxElementId);
    $el.prop('fluxElementIndex', i);
    $el.prop('fluxElementId', c.fluxElementId);
    $el.addClass(c.plugin.getIdentifier());
    $el.appendTo(_this.$container);
    c.fluxElementIndex = i;
    c.$el = $el;
  });

  this.emit('onRenderEditableContents', this);
  this.renderPlusWidgets();
};

FluxEditor.prototype.getFluxEditorUniqId = function() {
  this.fluxEditorUniqIndex += 1;
  return this.fluxEditorUniqIndex;
}


// // FluxEditor.prototype.disable = function(plugins) {
// //   this.$container.removeClass('flux-editor');
// //   var html = this.getHTML();
// //   this.$container.empty();
// //   this.$container.html(html);
// // }


FluxEditor.prototype.renderPlusWidgets = function(index) {
  var _this = this;
  var $plus;
  $('.flux-editor-plus').remove();

  $.each(this.contents, function(i, c) {
    if (i == 0) {
      _this.getPlusWidget(-1).insertBefore(c.$el);
    }
    _this.getPlusWidget(i).insertAfter(c.$el);
  });
}

FluxEditor.prototype.renderEditableContent = function(index) {
  var _this = this;
  var content = this.contents[index];
  var $el = content.plugin.getEditableElement(content, this);
    $el.attr('id', 'flux-editor-element_' + content.fluxElementId);
    $el.prop('fluxElementIndex', content.fluxElementIndex);
    $el.prop('fluxElementId', content.fluxElementId);
    $el.addClass(content.plugin.getIdentifier());
  content.$el.replaceWith($el);
  content.$el = $el;

  this.emit('onRenderEditableContent', this);
  return $el;
};

FluxEditor.prototype.moveEditableContent = function(index, direction) {
  this.moveContent(index, direction);
  this.renderEditableContents();

  // if (direction == 'up' && index > 0) {
  //   // this.renderEditableContent(index);
  //   // this.renderEditableContent(index - 1);
  // } else if (direction == 'down' && index < (this.contents.length - 1)) {
  //   // this.renderEditableContent(index);
  //   // this.renderEditableContent(index + 1);
  // }
}

FluxEditor.prototype.insertNewEditableContentAfter = function(index, plugin, properties) {
  var newIndex = index + 1;
  this.contents.splice(index + 1, 0, properties);
  this.contents[newIndex].plugin = plugin;
  this.contents[newIndex].fluxElementId = this.getFluxEditorUniqId();

  var $el = plugin.getEditableElement(this.contents[newIndex], this);
  this.contents[newIndex].$el = $el;

  if (this.contents[index]) {
    $el.insertAfter(this.contents[index].$el);
  } else if(index == -1) {
    $el.insertBefore(this.contents[1].$el);
  }

  this.setContentIndexes();
  $el.attr('id', `flux-editor-element_${this.contents[newIndex].fluxElementId}`);
  $el.addClass(plugin.getIdentifier());
  this.renderPlusWidgets();

  return this.contents[newIndex];
}

FluxEditor.prototype.deleteEditableContent = function(index) {
  var content = this.contents[index];
  content.$el.remove();
  this.deleteContent(index);
  this.setContentIndexes();
  this.renderPlusWidgets();
}

FluxEditor.prototype.setContentIndexes = function() {
  $.each(this.contents, function(i, c) {
    c.fluxElementIndex = i;
    c.$el.prop('fluxElementIndex', i);
    c.$el.prop('fluxElementId', c.fluxElementId);
  })
}

FluxEditor.prototype.getPlusWidget = function(index) {
  var _this = this;
  var $plus = $(require('./templates/plus-widget.html'));
  var $a = $plus.find('a');

  $plus.click(function(e) {
    _this.removeTooltip();
  });

  $a.click(function(e) {
    if (!$(this).hasClass('open')) {
      $('.flux-editor-plus-button').removeClass('open');
      $(this).addClass('open');

      var tooltip = new FluxEditorTooltip();
      $.each(_this.registeredPlugins, function(i, p) {
        if (typeof p.instance.getTooltipItem == 'function') {
          // TODO add conditions to test if item is valid
          tooltip.addItem(p.instance.getTooltipItem({
            core: _this,
            fluxElementIndex: index
          }));
        }
      });

      tooltip.display($(this).offset().top - 50, $(this).offset().left + 9)
    } else {
      _this.removeTooltip();
      $(this).removeClass('open');
    }
    return false;
  });

  $(Object.keys(this.registeredPlugins).join(', ')).mousedown(function() {
    $('.flux-editor-plus-button').removeClass('open');
    _this.removeTooltip();
  });

  return $plus;
}

FluxEditor.prototype.getHTML = function() {
  var _this = this;
  var html = [];

  $.each(this.contents, function(i, c) {
    html.push(c.plugin.getHTML(c, _this));
  });

  return html.join('\n');
}


FluxEditor.prototype.getElement = function(index) {
  if (this.contents[index]) {
    return this.contents[index].$el;
  }
}

FluxEditor.prototype.getSamePreviousElement = function(index, indentifier) {
  var $el = null
  if (index > 0 && indentifier) {
    for (var i = (index - 1); i >= 0; i--) {
      $el = this.getElement(i);
      if ($el.hasClass(indentifier)) {
        return $el;
      }
    }
  }
  return $el;
}

FluxEditor.prototype.getSameNextElement = function(index, indentifier) {
  var $el = null
  if (index>=0 && indentifier) {
    for (var i = (index + 1); i < this.contents.length; i++) {
      $el = this.getElement(i);
      if ($el.hasClass(indentifier)) {
        return $el;
      }
    }
  }
  return $el;
}

FluxEditor.prototype.updateContent = function(index) {
  var _this = this;
  if (this.contents[index] && this.contents[index].plugin.getIdentifier() != 'flux-editor-uneditable') {
    $.each(this.contents[index].plugin.getContent(this.getElement(index)[0]), function(k, v) {
      _this.contents[index][k] = v;
    })
  }
}

FluxEditor.prototype.addContent = function(index, properties, replace) {
  replace = (replace === true ? 1 : 0);
  //  TODO add check for minipal properties requirements

  this.contents.splice(index, replace, properties);
  this.emit('onNewElement', {
    core: this,
    index: index,
    properties: properties
  });
}

FluxEditor.prototype.replaceContent = function(index, properties) {
  this.addContent(index, properties, true);
}

FluxEditor.prototype.moveContent = function(index, direction) {
  if (direction == 'up' && index > 0) {
    var content = this.contents.splice(index, 1)[0];
    this.contents.splice(index - 1, 0, content);
  } else if (direction == 'down' && index < (this.contents.length - 1)) {
    var content = this.contents.splice(index, 1)[0];
    this.contents.splice(index + 1, 0, content);
  }
  this.setContentIndexes();
}

FluxEditor.prototype.deleteContent = function(index) {
  this.contents.splice(index, 1)
}

FluxEditor.prototype.removeTooltip = function() {
  new FluxEditorTooltip().remove();
}

global.FluxEditor = FluxEditor;

module.exports = FluxEditor;
