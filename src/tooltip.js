require('./less/tooltip.less');

function FluxEditorTooltip() {
  this.selector = '#flux-editor-tooltip';
  this.$html = $(require('./templates/tooltip.html'));
}

FluxEditorTooltip.prototype.remove = function(top, left) {
  $(this.selector).remove();
}

FluxEditorTooltip.prototype.addItem = function(item) {
  var _this = this;
  if (item.separator) {
    var $a = $('<div class="flux-editor-tooltip-separator"></div>').appendTo($('.flux-editor-tooltip-box', this.$html));
  } else {
    var $a = $('<a href="#" class="flux-editor-tooltip-item"></a>').appendTo($('.flux-editor-tooltip-box', this.$html));
    if (item.className) {
      $a.addClass(item.className);
    }
    if (item.title) {
      $a.attr('title', item.title);
    }
    $a.click(function(e) {
      if (item.method && item.plugin && typeof item.plugin[item.method] == 'function') {
        item.plugin[item.method](item.arguments)
      } else if(typeof item.callback == 'function') {
        item.callback(_this);
      }
      if (!item.sticky) {
        _this.remove();
      }
      return false;
    })
  }
}

FluxEditorTooltip.prototype.display = function(top, left, hideTriangle, editorMode) {
  this.remove();
  var $t = this.$html.appendTo('body');
  if (hideTriangle) {
    this.$html.find('.flux-editor-tooltip-triangle').hide();
  }
  if (editorMode) {
   this.$html.addClass('flux-editor-tooltip-variant-editor');
  }

  var w = 0;
  $('.flux-editor-tooltip-box').find('>a, >div').each(function() {
    w += $(this).outerWidth(true);
  });
  $('.flux-editor-tooltip-box').css('width', w + 2);

  left = left - ($t.outerWidth(true) / 2)
  if (left < 0) {
    left = 5;
  }

  if (left + $t.outerWidth(true) > $('body').width()) {
    left = $('body').width() - $t.outerWidth(true) -5;
  }

  $t.css({
    visibility: 'visible',
    top: top,
    left: left,
  });

}

global.FluxEditorTooltip = FluxEditorTooltip;

module.exports = FluxEditorTooltip;
