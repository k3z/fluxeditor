import '.././less/plugin-twitter.less';

function FluxEditorTwitter(options) {
  this.defaults = {
    addTemplate: require('.././templates/twitter-add.html'),
    selectors: ['figure[class~="flux-twitter"]'],
    identifier: 'flux-editor-twitter',
    tooltipItemTitle: 'New Tweet',
    formPlaceholderText: 'Copy/paste Twitter short url here and press enter (esc to cancel)',
    events: {
      onNewElement: function(args) {}
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorTwitter.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorTwitter.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorTwitter.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorTwitter.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-twitter-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorTwitter.prototype.addNewElement = function(args) {
  args.core.insertNewEditableContentAfter(args.fluxElementIndex, this, {
    html: '',
  });
}

FluxEditorTwitter.prototype.getContent = function(el) {
  return {
    html: el.innerHTML,
  }
}

FluxEditorTwitter.prototype.getHTML = function(content, core) {
  return `<figure class="flux-twitter">${content.html}</figure>`;
}

FluxEditorTwitter.prototype.getEditableElement = function(content, core) {
  var _this = this;
  if (!content.html.trim()) {
    var $el = $(this.settings.addTemplate);
    var $input = $el.find('.flux-editor-twitter-src');
    var $cancel = $el.find('.flux-editor-twitter-cancel');

    $input.attr('placeholder', this.settings.formPlaceholderText);
    $input.keyup(function(e) {
      // Sample https://twitter.com/twipsum/status/13007058604
      if (e.keyCode == 13) {
        var src = $(this).val();
        if (src) {
          $.ajax({
            url: 'https://api.twitter.com/1/statuses/oembed.json?align=center&url='+src,
            dataType: "jsonp",
            success: function(data){
              if (data.html) {
                content.html = data.html;
                core.renderEditableContent(content.fluxElementIndex);
              }
            }
          });
        }
      }

      if (e.keyCode == 27) { // esc
        core.deleteEditableContent(content.fluxElementIndex);
      }
    });

    $cancel.click(function(e) {
      core.deleteEditableContent(content.fluxElementIndex);
    })

    setTimeout(function() {
      $input.focus();
    }, 500);

  } else {
    var $el = $('<figure class="flux-editor-twitter"><div class="flux-editor-twitter-mask"></div></figure>').append(content.html);
    $el.css('min-height', 220);
    var $mask = $el.find('.flux-editor-twitter-mask');

    $mask.click(function(e){
      var tooltip = new FluxEditorTooltip();

      // move
      tooltip.addItem({
        className: 'flux-editor-twitter-tooltip-arrowup',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'up');
        }
      })
      tooltip.addItem({
        className: 'flux-editor-twitter-tooltip-arrowdown',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'down');
        }
      })

      // remove
      tooltip.addItem({
        className: 'flux-editor-twitter-tooltip-trash',
        callback: function() {
          core.deleteEditableContent(content.fluxElementIndex);
        }
      })

      tooltip.display(
        $mask.offset().top + ($mask.height() - 72),
        $mask.offset().left + ($mask.width() / 2),
        true
      );
    });
  }

  return $el;
}

global.FluxEditorTwitter = FluxEditorTwitter;

module.exports = FluxEditorTwitter;
