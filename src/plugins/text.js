import '.././less/plugin-text.less';
import '.././less/plugin-text-medium.less';

import MediumEditor from 'medium-editor';
import rangy from 'rangy';

var updateTimerID = null;
rangy.init();

function FluxEditorText(options) {
  this.defaults = {
    selectors: ['p', 'h2', 'h3', 'blockquote'],
    identifier: 'flux-editor-text',
    tooltipItemTitle: 'New text',
    events: {
      onEditLink: function(args) {
        console.debug(args);
      }
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorText.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorText.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorText.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorText.prototype.getContent = function(el) {
  return {
    html: el.innerHTML,
    tag: el.tagName.toLowerCase(),
    align: this.getTextAlignment($(el))
  }
}

FluxEditorText.prototype.getHTML = function(content, core) {
  var $el = $(`<${content.tag}></${content.tag}>`);
  $el.html(this.removeSurroundingTag(content.$el.html(), 'span', true));
  $el.addClass(content.align);
  return $el[0].outerHTML;
}


FluxEditorText.prototype.addNewElement = function(args) {
  var newContent = args.core.insertNewEditableContentAfter(args.fluxElementIndex, this, {
    html: '',
    tag: 'p',
    align: null
  });
  newContent.$el.focus();
}

FluxEditorText.prototype.getEditableElement = function(content, core) {

  var _this = this;
  content.html = (content.html == '' ? '<br>' : content.html);
  var $el = $(`<${content.tag.toLowerCase()}></${content.tag.toLowerCase()}>`).html(content.html);
  $el.addClass('clearfix');

  if (content.align) {
    $el.addClass(content.align);
  }

  $el.keydown(function(e) {

    if (e.keyCode == 13 && e.shiftKey == false) {
      if (_this.isElementEmpty($(this)) === false) {
        e.preventDefault();
        // workaround for medium bug in blockquote
        // https://github.com/yabwe/medium-editor/issues/1259
        if ($el.get(0).tagName.toLowerCase() == 'blockquote' && _this.getCaretPosition(this) == 'end') {
          _this.disableEditor($el);
          var newContent = core.insertNewEditableContentAfter(content.fluxElementIndex, _this, {
            html: '',
            tag: 'p',
            align: null
          });
          _this.enableEditor($el);
          newContent.$el.focus();
        } else {
          _this.splitContentAtCaret(content, core);
        }
      }
    } else if (e.keyCode == 13 && e.shiftKey == true) {
      document.execCommand("InsertHTML", false, '<br>');
    }

  });

  $el.keyup(function(e) {

    if (e.keyCode != 8) {
      $(this).removeClass('deletable')
    }

    if ((e.keyCode == 38 || e.keyCode == 40) && e.shiftKey == false) { // arrow up | down
      var position = _this.getCaretPosition(this);

      if (_this.isElementEmpty($(this)) && e.keyCode == 40) {
        position = 'end';
      }

      if (position == 'start' && content.fluxElementIndex > 0) {
        var $el = core.getSamePreviousElement(content.fluxElementIndex, _this.getIdentifier());
        if ($el) {
          $el.focus();
          _this.setCaretToEnd($el[0]);
        }
      }
      if (position == 'end') {
        var $el = core.getSameNextElement(content.fluxElementIndex, _this.getIdentifier());
        if ($el) {
          $el.focus();
        }

      }
    } else if (e.keyCode == 8) { //Backspace
      if (_this.isElementEmpty($(this)) === true) {
        var fluxElementIndex = content.fluxElementIndex;

        if ($(this).hasClass('deletable')) {
          core.deleteEditableContent(fluxElementIndex);

          if (fluxElementIndex > 0) {
            var $el = core.getElement(fluxElementIndex - 1);
            if ($el.hasClass(_this.getIdentifier())) {
              $el.focus();
              _this.setCaretToEnd($el[0]);
            }
          }
        }
        $(this).addClass('deletable')

      } else {
        console.debug('start')
        if (_this.getCaretPosition(this) == 'start') {
          var $previousEl = core.getSamePreviousElement(content.fluxElementIndex, _this.getIdentifier());
          if ($previousEl) {
            var previous_index = $previousEl.prop('fluxElementIndex');
            var previous_html = $previousEl.html();

            core.contents[previous_index].html = $(this).html();
            core.deleteEditableContent(content.fluxElementIndex);
            core.renderEditableContent(previous_index);

            $el = core.getElement(previous_index);
            $el.focus();

            document.execCommand("InsertHTML", false, previous_html)

          }
        }
      }
    } else if(e.keyCode == 46) {  // fn+Backspace
      var current_index = content.fluxElementIndex;
      var $el = core.getSameNextElement(current_index, _this.getIdentifier());
      if ($el) {
        var current_html = $(this).html();
        var next_index = $el.prop('fluxElementIndex');
        core.contents[current_index].html = core.contents[next_index].$el.html();
        core.deleteEditableContent(next_index);
        core.renderEditableContent(current_index);
        content.$el.focus();
        document.execCommand("InsertHTML", false, current_html)
      }
    }

  });

  this.enableEditor($el, content, core);

  return $el;
}

FluxEditorText.prototype.enableEditor = function($el, content, core) {
  var _this = this;
  var h2 = MediumEditor.Extension.extend({
    name: 'h2',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b>H2</b>';
      this.button.title = 'H2';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.changeContainerTag(core, content, $el, 'h2');

    }
  });
  var h3 = MediumEditor.Extension.extend({
    name: 'h3',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b>H3</b>';
      this.button.title = 'H3';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.changeContainerTag(core, content, $el, 'h3');

    }
  });
  var blockquote = MediumEditor.Extension.extend({
    name: 'blockquote',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b><i class="fa fa-quote-right"></i></b>';
      this.button.title = 'blockquote';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.changeContainerTag(core, content, $el, 'blockquote');

    }
  });

  var textLeft = MediumEditor.Extension.extend({
    name: 'textLeft',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b><i class="fa fa-align-left"></i></b>';
      this.button.title = 'Text align left';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.setTextAlignement(core, content, $el, 'text-left');
    }
  });

  var textCenter = MediumEditor.Extension.extend({
    name: 'textCenter',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b><i class="fa fa-align-center"></i></b>';
      this.button.title = 'Text align center';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.setTextAlignement(core, content, $el, 'text-center');
    }
  });

  var textRight = MediumEditor.Extension.extend({
    name: 'textRight',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<b><i class="fa fa-align-right"></i></b>';
      this.button.title = 'Text align right';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.setTextAlignement(core, content, $el, 'text-right');
    }
  });

  this.editor = new MediumEditor($el, {
    disableReturn: true,
    disableDoubleReturn: true,
    toolbar: {
      static: false,
      updateOnEmptySelection: false,
      buttons: ['h2', 'h3', 'blockquote', 'bold', 'italic', 'anchor', 'textLeft', 'textCenter', 'textRight'],
    },
    extensions: {
      h2: new h2(),
      h3: new h3(),
      blockquote: new blockquote(),
      textLeft: new textLeft(),
      textCenter: new textCenter(),
      textRight: new textRight()
    },
  });
}


FluxEditorText.prototype.disableEditor = function($el) {
  MediumEditor.getEditorFromElement($el[0]).destroy();
}

FluxEditorText.prototype.focus = function($el) {
 $el.focus();
}

FluxEditorText.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-text-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorText.prototype.splitContentAtCaret = function(content, core) {
  var beforeHtml = this.getHtmlAtCaret(content.$el[0]);
  var afterHtml = this.getHtmlAtCaret(content.$el[0], true);

  if ($(`<div>${beforeHtml}</div>`).text() == '') {
    beforeHtml = '';
  }

  if ($(`<div>${afterHtml}</div>`).text() == '') {
    afterHtml = '';
  }

  content.html = beforeHtml;
  core.renderEditableContent(content.fluxElementIndex);
  var newContent = core.insertNewEditableContentAfter(content.fluxElementIndex, this, {
    html: afterHtml,
    tag: 'p',
    align: null
  });

  newContent.$el.focus();
}

FluxEditorText.prototype.changeContainerTag = function(core, content, $el, tagName) {
  core.contents[content.fluxElementIndex].tag = (core.contents[content.fluxElementIndex].tag == tagName ? 'p': tagName);
  core.renderEditableContent(content.fluxElementIndex);
  content.$el.focus();
}

FluxEditorText.prototype.setTextAlignement = function(core, content, $el, alignment) {
  core.contents[content.fluxElementIndex].align = alignment;
  core.renderEditableContent(content.fluxElementIndex);
}

FluxEditorText.prototype.getTextAlignment = function($el) {
  if ($el.hasClass('text-center')) {
    return 'text-center';
  } else if ($el.hasClass('text-right')) {
    return 'text-right';
  } else {
    return 'text-left';
  }
}

FluxEditorText.prototype.isElementEmpty = function($el) {
  if ($el.text().length == 0) {
    return true
  }
  return false;
}

FluxEditorText.prototype.getHtmlAtCaret = function(el, after) {
  var html = '';
  var sel = rangy.getSelection();
  if (sel.rangeCount > 0) {
    var selRange = sel.getRangeAt(0);
    var range = rangy.createRange();
    range.selectNodeContents(el);
    if (!after) {
      range.setEnd(selRange.startContainer, selRange.startOffset);
    } else {
      range.setStart(selRange.startContainer, selRange.startOffset);
    }

    var frag = range.cloneContents();
    var el = document.createElement("body");
    el.appendChild(frag);
    html = el.innerHTML;
  }
  return html;
};

// source http://stackoverflow.com/questions/7451468/contenteditable-div-how-can-i-determine-if-the-cursor-is-at-the-start-or-end-o
FluxEditorText.prototype.getCaretPosition = function(el) {
  var sel = rangy.getSelection();
  if (sel.rangeCount) {
      var selRange = sel.getRangeAt(0);
      var testRange = selRange.cloneRange();

      testRange.selectNodeContents(el);
      testRange.setEnd(selRange.startContainer, selRange.startOffset);
      var atStart = (testRange.toString().trim() == "");

      testRange.selectNodeContents(el);
      testRange.setStart(selRange.endContainer, selRange.endOffset);
      var atEnd = (testRange.toString().trim() == "");
  }

  if (atStart) {
    return 'start';
  } else if (atEnd) {
    return 'end';
  } else {
    return null;
  }
}

FluxEditorText.prototype.setCaretToEnd = function(el) {
  var range = rangy.createRange();
  range.selectNodeContents(el);
  var sel = rangy.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
  sel.collapseToEnd();
}

FluxEditorText.prototype.removeSurroundingTag = function(html, tag, nested) {
  var $html = $('<div></div>').html(html);
  var $tags = $html.find(tag);
  if ($tags) {
    var $el = $($tags[0]);
    var html = $el.html();
    $el.replaceWith(html);
  }
  if ($tags.length>1 && nested === true) {
    $html = this.removeSurroundingTag($html.html(), tag, nested);
  }
  return $html.html();
}

global.FluxEditorText = FluxEditorText;

module.exports = FluxEditorText;
