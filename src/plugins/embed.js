import '.././less/plugin-embed.less';

var updateTimerID;

function FluxEditorEmbed(options) {
  this.defaults = {
    codeTemplate: require('.././templates/embed-code.html'),
    selectors: ['figure[class~="flux-embed"]'],
    identifier: 'flux-editor-embed',
    tooltipItemTitle: 'New Tweet',
    formPlaceholderText: 'Paste your embed code here',
    events: {
      onNewElement: function(args) {}
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorEmbed.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorEmbed.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorEmbed.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorEmbed.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-embed-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorEmbed.prototype.addNewElement = function(args) {
  args.core.insertNewEditableContentAfter(args.fluxElementIndex, this, {
    html: '',
  });
}

FluxEditorEmbed.prototype.getContent = function(el) {
  return {
    html: this.cleanup(el.innerHTML),
  }
}

FluxEditorEmbed.prototype.getHTML = function(content, core) {
  return `<div class="flux-embed">${content.html}</div>`;
}

FluxEditorEmbed.prototype.getEditableElement = function(content, core) {
  var _this = this;
  var $el = $(this.settings.codeTemplate);
  var $preview = $el.find('.flux-editor-embed-preview');
  var $mask = $preview.find('.flux-editor-embed-mask');
  var $code = $preview.find('.flux-editor-embed-code');
  var $form = $el.find('.flux-editor-embed-form');
  var $textarea = $form.find('textarea');
  var $actions = $el.find('.flux-editor-embed-actions');
  var $hide = $el.find('.flux-editor-embed-action-hide');
  var $refresh = $el.find('.flux-editor-embed-action-refresh');

  $textarea.attr('placeholder', this.settings.formPlaceholderText);
  $code.html(content.html);

  if (!content.html.trim()) {
    $preview.addClass('hide');
    $form.removeClass('hide');

    $hide.click(function(e){
      if ($textarea.val().trim() == '') {
        core.deleteEditableContent(content.fluxElementIndex);
      } else {
        core.contents[content.fluxElementIndex].html = $textarea.val();
        core.renderEditableContent(content.fluxElementIndex);
      }
      return false;
    });
    $refresh.click(function(e){
      if ($textarea.val().trim()) {
        $preview.removeClass('hide');
        core.contents[content.fluxElementIndex].html = $textarea.val();
        $code.html(core.contents[content.fluxElementIndex].html);
      }
      return false;
    });

  } else {
    $mask.click(function(e){
      var tooltip = new FluxEditorTooltip();

      // format
      tooltip.addItem({
        className: 'flux-editor-embed-tooltip-edit',
        callback: function() {
          $form.toggleClass('hide');
          $textarea.focus();
        }
      });

      // move
      tooltip.addItem({
        className: 'flux-editor-embed-tooltip-arrowup',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'up');
        }
      })
      tooltip.addItem({
        className: 'flux-editor-embed-tooltip-arrowdown',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'down');
        }
      })

      // remove
      tooltip.addItem({
        className: 'flux-editor-embed-tooltip-trash',
        callback: function() {
          core.deleteEditableContent(content.fluxElementIndex);
        }
      })

      tooltip.display(
        $mask.offset().top + ($mask.height() - 50),
        $mask.offset().left + ($mask.width() / 2),
        true
      );
    });

    $textarea.val(content.html.trim());
    // var rows = content.html.trim().split(/\r\n|\r|\n/).length + 2;
    // $textarea.attr('rows', (rows <= 14 ? rows : 14));

    $textarea.on('focus blur paste', function(e) {
      core.contents[content.fluxElementIndex].html = $textarea.val();
    });

    $textarea.on('keyup input', function(e) {
      window.clearTimeout(updateTimerID);
      updateTimerID = setTimeout(function() {
        core.contents[content.fluxElementIndex].html = $textarea.val();
      }, 100);
    });

    $hide.click(function(e){
      $form.addClass('hide');
      return false;
    });

    $refresh.click(function(e){
      $code.html(core.contents[content.fluxElementIndex].html);
      return false;
    });
  }

  return $el;
}

FluxEditorEmbed.prototype.cleanup = function(html) {
  $el = $('<div></div>').html(html);
  $el.find('script').each(function(k, s) {
    $(s).empty();
  });

  return $el.html();
}

global.FluxEditorEmbed = FluxEditorEmbed;

module.exports = FluxEditorEmbed;
