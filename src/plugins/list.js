import '.././less/plugin-list.less';
import '.././less/plugin-text-medium.less';

import MediumEditor from 'medium-editor';
import rangy from 'rangy';

var updateTimerID = null;
rangy.init();

function FluxEditorList(options) {
  this.defaults = {
    selectors: ['ul', 'ol'],
    identifier: 'flux-editor-list',
    tooltipItemTitle: 'New text',
    events: {
      onEditLink: function(args) {
        console.debug(args);
      }
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorList.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorList.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorList.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorList.prototype.getContent = function(el) {
  var html = []

  html.push('<ul>');
  $('li', el).each(function(i, li) {
    html.push(`<li>${$(li).html()}</li>`)
  });
  html.push('</ul>');

  return {
    html: html,
    tag: el.tagName.toLowerCase(),
    align: null
  }
}

FluxEditorList.prototype.getHTML = function(content, core) {
  var _this = this;
  var $el = $(`<${content.tag}></${content.tag}>`);

  $('li', content.$el).each(function(i, li) {
    var $li = $(li);
    _this.disableEditor($li)
    $('<li></li>').html($li.html()).appendTo($el)
  });

  $el.addClass(content.align);
  return $el[0].outerHTML;
}


FluxEditorList.prototype.addNewElement = function(args) {
  var newContent = args.core.insertNewEditableContentAfter(args.fluxElementIndex, this, {
    html: '<li></li>',
    tag: 'ul',
    align: null
  });
  $('li', newContent.$el).first().focus();
}

FluxEditorList.prototype.getEditableElement = function(content, core) {

  var _this = this;
  content.html = (content.html == '' ? '<br>' : content.html);
  var $el = $(`<${content.tag.toLowerCase()}></${content.tag.toLowerCase()}>`).html(content.html);
  $el.addClass('clearfix');

  if (content.align) {
    $el.addClass(content.align);
  }

  // Set tabindex="0" to get focus on li
  $('li', $el).each(function(i, li) {
    $(li).attr('tabindex', 0);
    $(li).attr('index', i);
  });

  $($el).on('keydown', 'li', function(e) {
    if (e.keyCode == 13 && e.shiftKey == false) {
      _this.splitItemAtCaret($(this), content, core);
    }
  });

  $($el).on('keyup', 'li', function(e) {

    if ((e.keyCode == 38 || e.keyCode == 40) && e.shiftKey == false) { // arrow up | down
      e.preventDefault();
      var $this = $(this);
      var $next = $this.next();
      var $prev = $this.prev();
      var position = _this.getCaretPosition(this);

      if (_this.isElementEmpty($(this)) && e.keyCode == 40) {
        position = 'end';
      }

      if (position == 'start' && $prev.length > 0) {
        $prev.focus();
        _this.setCaretToEnd($prev[0]);
      }

      if (position == 'end' && $next.length > 0) {
        $next.focus();
      }

      // if (position == 'start' && content.fluxElementIndex > 0 && $prev.length == 0) {
      //   var $el = core.getSamePreviousElement(content.fluxElementIndex, _this.getIdentifier());
      //   if ($el) {
      //     $el.focus();
      //     _this.setCaretToEnd($el[0]);
      //   }
      // }

      // if (position == 'end' && $next.length == 0) {
      //   var $el = core.getSameNextElement(content.fluxElementIndex, _this.getIdentifier());
      //   if ($el) {
      //     $el.focus();
      //   }
      // }
    }

    if (e.keyCode != 8) {
      $(this).removeClass('deletable')
    }

    if (e.keyCode == 8) { //Backspace
      var $this = $(this);
      var $next = $this.next();
      var $prev = $this.prev();

      if (_this.isElementEmpty($this) === true && $prev.length > 0) {
        if ($this.hasClass('deletable')) {
          $prev.focus();
          _this.setCaretToEnd($prev[0]);
          $this.remove();
        }
        $this.addClass('deletable')
      } else {
        if (_this.getCaretPosition(this) == 'start' && $prev.length > 0) {
            var prev_html = $prev.html();
            var html = $this.html();
            $prev.empty();
            $prev.focus();
            document.execCommand("InsertHTML", false, prev_html)
            $prev.focus();
            var sel = rangy.getSelection();
            var b = sel.getBookmark();
            document.execCommand("InsertHTML", false, html)
            sel.moveToBookmark(b);
            $this.remove();
        }

        if (_this.isElementEmpty($this) === true && $next.length == 0  && $prev.length == 0) {
          var fluxElementIndex = content.fluxElementIndex;
          core.deleteEditableContent(fluxElementIndex);

          if (fluxElementIndex > 0) {
            var $el = core.getElement(fluxElementIndex - 1);

            if ($el.length > 0) {
              $el.focus();
              _this.setCaretToEnd($el[0]);
            }
            if ($el.hasClass(_this.getIdentifier())) {
            }
          }

        }
      }
    }

    if(e.keyCode == 46) {  // fn+Backspace

      var $this = $(this);
      var $next = $this.next();

      if ($next.length > 0) {
        var sel = rangy.getSelection();
        var b = sel.getBookmark();
        document.execCommand("InsertHTML", false, $next.html())
        sel.moveToBookmark(b);
        $next.remove();
      }
    }

  });

  this.enableEditor($('li', $el), content, core);

  return $el;
}

FluxEditorList.prototype.enableEditor = function($el, content, core) {
  var _this = this;

  var ol = MediumEditor.Extension.extend({
    name: 'ol',
    init: function () {
      this.button = this.document.createElement('button');
      this.button.classList.add('medium-editor-action');
      this.button.innerHTML = '<span class="flux-editor-list-editor-tooltip-item-ol"></span>';
      this.button.title = 'OL';
      this.on(this.button, 'click', this.handleClick.bind(this));
    },
    getButton: function () {
      return this.button;
    },
    handleClick: function (event) {
      _this.changeContainerTag(core, content, $el, 'ol');
      return false;
    }
  });

  this.editor = new MediumEditor($el, {
    disableReturn: true,
    disableDoubleReturn: true,
    toolbar: {
      static: false,
      updateOnEmptySelection: false,
      buttons: ['ol', 'bold', 'italic', 'anchor'],
    },
    extensions: {
      ol: new ol(),
    }
  });
}


FluxEditorList.prototype.disableEditor = function($el) {
  var inst = MediumEditor.getEditorFromElement($el[0])
  if (inst) {
    inst.destroy();
  }
}

FluxEditorList.prototype.focus = function($el) {
 $el.focus();
}

FluxEditorList.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-list-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorList.prototype.splitItemAtCaret = function($li, content, core) {
  var _this = this;
  var beforeHtml = this.getHtmlAtCaret($li[0]);
  var afterHtml = this.getHtmlAtCaret($li[0], true);

  if ($(`<div>${beforeHtml}</div>`).text() == '') {
    beforeHtml = '';
  }

  if ($(`<div>${afterHtml}</div>`).text() == '') {
    afterHtml = '';
  }

  $li.html(beforeHtml);
  var $new_el = $('<li tabindex="0"></li>');
  $new_el.html(afterHtml);

  $li.after($new_el);
  _this.enableEditor($new_el, content, core);
  $new_el.focus();
}

FluxEditorList.prototype.changeContainerTag = function(core, content, $el, tagName) {
  core.contents[content.fluxElementIndex].tag = (core.contents[content.fluxElementIndex].tag == tagName ? 'ul': tagName);
  core.renderEditableContent(content.fluxElementIndex);
  content.$el.focus();
}


FluxEditorList.prototype.isElementEmpty = function($el) {
  if ($el.text().length == 0) {
    return true
  }
  return false;
}

FluxEditorList.prototype.getHtmlAtCaret = function(el, after) {
  var html = '';
  var sel = rangy.getSelection();
  if (sel.rangeCount > 0) {
    var selRange = sel.getRangeAt(0);
    var range = rangy.createRange();
    range.selectNodeContents(el);
    if (!after) {
      range.setEnd(selRange.startContainer, selRange.startOffset);
    } else {
      range.setStart(selRange.startContainer, selRange.startOffset);
    }

    var frag = range.cloneContents();
    var el = document.createElement("body");
    el.appendChild(frag);
    html = el.innerHTML;
  }
  return html;
};

// source http://stackoverflow.com/questions/7451468/contenteditable-div-how-can-i-determine-if-the-cursor-is-at-the-start-or-end-o
FluxEditorList.prototype.getCaretPosition = function(el) {
  var sel = rangy.getSelection();
  if (sel.rangeCount) {
      var selRange = sel.getRangeAt(0);
      var testRange = selRange.cloneRange();

      testRange.selectNodeContents(el);
      testRange.setEnd(selRange.startContainer, selRange.startOffset);
      var atStart = (testRange.toString().trim() === "");

      testRange.selectNodeContents(el);
      testRange.setStart(selRange.endContainer, selRange.endOffset);
      var atEnd = (testRange.toString().trim() === "");
  }

  if (atStart) {
    return 'start';
  } else if (atEnd) {
    return 'end';
  } else {
    return null;
  }
}

FluxEditorList.prototype.setCaretToEnd = function(el) {
  var range = rangy.createRange();
  range.selectNodeContents(el);
  var sel = rangy.getSelection();
  sel.removeAllRanges();
  sel.addRange(range);
  sel.collapseToEnd();
}

FluxEditorList.prototype.removeSurroundingTag = function(html, tag, nested) {
  var $html = $('<div></div>').html(html);
  var $tags = $html.find(tag);
  if ($tags) {
    var $el = $($tags[0]);
    var html = $el.html();
    $el.replaceWith(html);
  }
  if ($tags.length>1 && nested === true) {
    $html = this.removeSurroundingTag($html.html(), tag, nested);
  }
  return $html.html();
}

global.FluxEditorList = FluxEditorList;

module.exports = FluxEditorList;
