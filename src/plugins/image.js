import '.././less/plugin-image.less';

function FluxEditorImage(options) {
  this.defaults = {
    editTemplate: require('.././templates/image-edit-widget.html'),
    widgetTemplate: require('.././templates/image-widget.html'),
    selectors: ['figure[class~="flux-image"]'],
    identifier: 'flux-editor-image',
    tooltipItemTitle: 'New image',
    linkPlaceholder: 'Copy/paste your link here and press enter.',
    events: {
      onNewElement: function(args) {}
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorImage.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorImage.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorImage.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorImage.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-image-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorImage.prototype.addNewElement = function(args) {
  this.emit('onNewElement', {
    plugin: this,
    fluxElementIndex: args.fluxElementIndex,
    core: args.core
  });
}

FluxEditorImage.prototype.insertNewElement = function(properties, index, core) {
  if (properties.src) {
    core.insertNewEditableContentAfter(index, this, {
      src: properties.src,
      caption: properties.caption
    });
  }
}

FluxEditorImage.prototype.getContent = function(el) {
  var $img = $(el).find('img');
  var $caption = $(el).find('figcaption');
  var $a = $(el).find('a');

  return {
    src: $img.attr('src'),
    caption: $caption.html() ? $caption.html() : null,
    href: $a.attr('href') ? $a.attr('href') : null
  }
}

FluxEditorImage.prototype.getHTML = function(content, core) {
  var $figure = $(this.settings.widgetTemplate);
  $figure.find('img').attr('src', content.src);


  if (content.href) {
     $figure.find('a').attr('href', content.href)
  } else {
    $figure.find('img').unwrap();
  }

  if (content.caption) {
    $figure.find('figcaption').html(content.caption);
  } else {
    $figure.find('figcaption').remove()
  }

  return $figure[0].outerHTML;
}

FluxEditorImage.prototype.getEditableElement = function(content, core) {
  var _this = this;
  var $figure = $(this.settings.editTemplate);
  var $img = $figure.find('img');
  var $imgContainer = $figure.find('.flux-editor-image-container');
  var $caption = $figure.find('figcaption');
  var $link = $figure.find('.flux-editor-image-link-value');
  var $linkDisplay = $figure.find('.flux-editor-image-link-display');

  $img.attr('src', content.src);
  $caption.html(content.caption);
  $link.val(content.href);
  $link.attr('placeholder', this.settings.linkPlaceholder);

  $linkDisplay.css('top', $img.position().top)

  $linkDisplay.attr('href', content.href)
  $linkDisplay.attr('title', `Go to ${content.href}`)

  $img.click(function(e){
    var tooltip = new FluxEditorTooltip();

    // link
    tooltip.addItem({
      className: 'flux-editor-image-tooltip-link',
      callback: function() {
        $link.parent().removeClass('hide')
        $link.focus()
      }
    });

    // move
    tooltip.addItem({
      className: 'flux-editor-image-tooltip-arrowup',
      callback: function() {
        core.moveEditableContent(content.fluxElementIndex, 'up');
      }
    });
    tooltip.addItem({
      className: 'flux-editor-image-tooltip-arrowdown',
      callback: function() {
        core.moveEditableContent(content.fluxElementIndex, 'down');
      }
    });

    // remove
    tooltip.addItem({
      className: 'flux-editor-image-tooltip-trash',
      callback: function() {
        core.deleteEditableContent(content.fluxElementIndex);
      }
    });

    tooltip.display(
      $img.offset().top + ($img.height() - 50),
      $img.offset().left + ($img.width() / 2),
      true
    );
  })

  $imgContainer.mouseenter(function() {
    if ($link.val()) {
      $linkDisplay.removeClass('hide')
    }
  });

  $imgContainer.mouseleave(function() {
    $linkDisplay.addClass('hide')
  });

  $link.on('keyup', function(e) {
    if (e.keyCode === 13) {
      $link.parent().addClass('hide')
      content.href = $link.val()
      $linkDisplay.attr('href', $link.val())
      core.updateContent(content.fluxElementIndex);
    }
  })

  $figure.find('.flux-editor-image-link-confirm').on('click', function(e) {
    $link.parent().addClass('hide')
    content.href = $link.val()
    $linkDisplay.attr('href', $link.val())
    core.updateContent(content.fluxElementIndex);
    return false;
  })

  $figure.find('.flux-editor-image-link-cancel').on('click', function(e) {
    $link.parent().addClass('hide')
    return false;
  })

  // caption
  $caption.addClass('flux-editor-image-caption');
  $caption.attr('placeholder', 'Add caption');
  $caption.mouseenter(function() {
    $(this).attr('contenteditable', 'true');
  });

  $caption.mouseleave(function() {
    $(this).removeAttr('contenteditable');
    $(this).blur();
    core.updateContent(content.fluxElementIndex);
  });

  $caption.on('paste', function(e) {
    e.stopPropagation();
    e.preventDefault();

    clipboardData = e.originalEvent.clipboardData || window.clipboardData;
    pastedData = clipboardData.getData('Text');
    var $pasted = $('<span></span>').html(pastedData);
    core.insertAtCaret($pasted[0]);
    core.updateContent(content.fluxElementIndex);

    return false;
  });

  return $figure;
}

global.FluxEditorImage = FluxEditorImage;

module.exports = FluxEditorImage;
