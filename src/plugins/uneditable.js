require('.././less/plugin-uneditable.less');

function FluxEditorUneditable(options={}) {
  this.defaults = {
    identifier: 'flux-editor-uneditable'
  };
  this.settings = $.extend({}, this.defaults, options);
}

FluxEditorUneditable.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorUneditable.prototype.getContent = function(el) {
  return {
    html: el.outerHTML,
  }
}

FluxEditorUneditable.prototype.getHTML = function(content, core) {
  return content.html;
}

FluxEditorUneditable.prototype.getEditableElement = function(content, core) {
  return $('<div><div class="flux-editor-uneditable-mask"></div></div>').append(content.html);
}

global.FluxEditorUneditable = FluxEditorUneditable;

module.exports = FluxEditorUneditable;
