import '.././less/plugin-video.less';

// Thanks to https://github.com/Zod-/jsVideoUrlParser for provider parsing
function FluxEditorVideo(options) {
  this.defaults = {
    providers: {
      'YouTube' : {
        'domains': ['youtu.be', 'youtube.com'],
        'match': /^.*(?:(?:youtu\.be\/|v\/|vi\/|u\/\w\/|embed\/)|(?:(?:watch)?\?v(?:i)?=|\&v(?:i)?=))([^#\&\?]*).*/,
        'player': 'https://www.youtube.com/embed/'
      },
      'dailymotion' : {
        'domains': ['dai.ly', 'dailymotion.com'],
        'match': /(?:\/video|ly)\/([A-Za-z0-9]+)/i,
        'player': 'http://www.dailymotion.com/embed/video/'
      },
      'vimeo' : {
        'domains': ['vimeo.com'],
        'match': /(?:\/(?:channels\/[\w]+|(?:(?:album\/\d+|groups\/[\w]+)\/)?videos?))?\/(\d+)/i,
        'player': 'https://player.vimeo.com/video/'
      }
    },
    editableTemplate: require('.././templates/video-editable.html'),
    addTemplate: require('.././templates/video-add.html'),
    widgetTemplate: require('.././templates/video-widget.html'),
    selectors: ['figure[class~="flux-video"]'],
    identifier: 'flux-editor-video',
    tooltipItemTitle: 'New Video',
    formPlaceholderTextTpl: 'Copy/paste {providers} video short url here and press enter (esc to cancel)',
    events: {
      onNewElement: function(args) {}
    }
  };
  this.settings = $.extend({}, this.defaults, options);

  FluxEditorEvents(this);
  this.registerEvents();
}

FluxEditorVideo.prototype.getSelectors = function(el) {
  return this.settings.selectors;
}

FluxEditorVideo.prototype.getIdentifier = function(el) {
  return this.settings.identifier;
}

FluxEditorVideo.prototype.registerEvents = function(plugins) {
  var _this = this;
  if (typeof this.settings.events == 'object') {
    $.each(this.settings.events, function(i, f) {
      _this.on(i, f);
    });
  }
}

FluxEditorVideo.prototype.getTooltipItem = function(context) {
  return {
    title: this.settings.tooltipItemTitle,
    className: 'flux-editor-video-tooltip-item',
    plugin: this,
    method: 'addNewElement',
    arguments: context
  }
}

FluxEditorVideo.prototype.addNewElement = function(args) {
  args.core.insertNewEditableContentAfter(args.fluxElementIndex, this, {
    player: null,
    format: '16by9',
    caption: ''
  });
}

FluxEditorVideo.prototype.getContent = function(el) {
  var $el = $(el.outerHTML);
  var $responsive = $el.find('.embed-responsive');
  var $iframe = $el.find('iframe');
  var $caption = $el.find('figcaption');
  var player = ($iframe ? $iframe.attr('src') : null);

  return {
    player: player,
    format: this.getVideoFormat($responsive),
    caption: $caption.text()
  }
}

FluxEditorVideo.prototype.getHTML = function(content, core) {
  var $el = $(this.settings.widgetTemplate);
  var $responsive = $el.find('.embed-responsive');
  var $iframe = $el.find('iframe');
  var $figcation = $el.find('figcaption');

  $iframe.attr('src', content.player);
  $responsive.addClass(`embed-responsive-${content.format}`);
  if (content.caption) {
    $figcation.html(content.caption);
  } else {
    $figcation.remove();
  }

  return $el[0].outerHTML;
}

FluxEditorVideo.prototype.getEditableElement = function(content, core) {
  // console.debug(content);
  var _this = this;
  var $template;

  if (!content.player) {
    $template = $(this.settings.addTemplate);
    var $input = $template.find('.flux-editor-video-src');
    var $cancel = $template.find('.flux-editor-video-cancel');

    var providers = Object.keys(this.settings.providers).join(' | ');
    $input.attr('placeholder', this.settings.formPlaceholderTextTpl.replace('{providers}', providers));
    $input.keyup(function(e) {
      if (e.keyCode == 13) {
        var src = $(this).val();

        var provider = _this.getVideoProvider(src);
        var player = _this.getVideoPlayerSrc(_this.settings.providers[provider], src);
        console.debug(player);

        if (player) {
          content.player = player;
          core.renderEditableContent(content.fluxElementIndex);
        }
      }

      if (e.keyCode == 27) { // esc
        core.deleteEditableContent(content.fluxElementIndex);
      }
    });

    $cancel.click(function(e) {
      core.deleteEditableContent(content.fluxElementIndex);
    })

    setTimeout(function() {
      $input.focus();
    }, 500);

  } else {
    $template = $(this.settings.editableTemplate);
    var $mask = $template.find('.flux-editor-video-mask');
    var $responsive = $template.find('.embed-responsive');
    var $caption = $template.find('figcaption');
    var $iframe = $template.find('iframe');
    $iframe.attr('src', content.player);
    if (content.format) {
      $responsive.addClass(`embed-responsive-${content.format}`);
    } else {
      $responsive.addClass('embed-responsive-16by9');
    }

    $mask.click(function(e){
      var tooltip = new FluxEditorTooltip();

      // format
      tooltip.addItem({
        className: 'flux-editor-video-tooltip-format',
        callback: function() {
          if ($responsive.hasClass('embed-responsive-16by9')) {
            $responsive.removeClass('embed-responsive-16by9');
            $responsive.addClass('embed-responsive-4by3');
            core.contents[content.index].format = '4by3';
          } else {
            $responsive.removeClass('embed-responsive-4by3');
            $responsive.addClass('embed-responsive-16by9');
            core.contents[content.index].format = '16by9';
          }
        }
      });

      // move
      tooltip.addItem({
        className: 'flux-editor-video-tooltip-arrowup',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'up');
        }
      })
      tooltip.addItem({
        className: 'flux-editor-video-tooltip-arrowdown',
        callback: function() {
          core.moveEditableContent(content.fluxElementIndex, 'down');
        }
      })

      // remove
      tooltip.addItem({
        className: 'flux-editor-video-tooltip-trash',
        callback: function() {
          core.deleteEditableContent(content.fluxElementIndex);
        }
      })

      tooltip.display(
        $mask.offset().top + ($mask.height() - 72),
        $mask.offset().left + ($mask.width() / 2),
        true
      );
    });

    // caption
    $caption.html(content.caption);
    $caption.mouseenter(function() {
      $(this).attr('contenteditable', 'true');
    });

    $caption.mouseleave(function() {
      $(this).removeAttr('contenteditable');
      $(this).blur();
      core.updateContent(content.index);
    });

    $caption.on('paste', function(e) {
      e.stopPropagation();
      e.preventDefault();

      clipboardData = e.originalEvent.clipboardData || window.clipboardData;
      pastedData = clipboardData.getData('Text');
      var $pasted = $('<span></span>').html(pastedData);
      core.insertAtCaret($pasted[0]);
      core.updateContent(content.fluxElementIndex);

      return false;
    });

  }

  return $template;
}

FluxEditorVideo.prototype.getVideoPlayerSrc = function(providerSettings, src) {
  if (providerSettings) {
    var r = src.match(providerSettings.match);
    return (r ? `${providerSettings.player}${r[1]}` : null);
  }
}

FluxEditorVideo.prototype.getVideoProvider = function(src) {
  var result;
  $.each(this.settings.providers, function(provider, p) {
    $.each(p.domains, function(i, d) {
      if (src.indexOf(d) >= 0) {
        result = provider;
      }
    })
  });
  return result;
}

FluxEditorVideo.prototype.getVideoFormat = function($responsive) {
  if ($responsive.hasClass('embed-responsive-4by3')) {
    return '4by3';
  }
  return '16by9';
}

global.FluxEditorVideo = FluxEditorVideo;

module.exports = FluxEditorVideo;
