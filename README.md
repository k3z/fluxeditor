# Flux Editor

A very opinionated content editor inspired by Medium and Spark.

## Install

    $ yarn install

    $ npm run serve

Open http://localhost:8080/

## Build

    $ npm run build

## Development version

    $ npm run development


## Publish demo

    $ ./do.sh publish_demo

Open http://flux-editor.k3z.fr/demo/
