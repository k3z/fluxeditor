
publish_demo() {
  rsync -chavzP --stats ./demo flux@lamp-lxc:/home/flux/editor
  rsync -chavzP --stats ./dist flux@lamp-lxc:/home/flux/editor
  rsync -chavzP --stats ./node_modules/jquery flux@lamp-lxc:/home/flux/editor/node_modules
  rsync -chavzP --stats ./node_modules/bootstrap flux@lamp-lxc:/home/flux/editor/node_modules
}


$*
